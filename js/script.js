"use strict";
/*
1. Метод forEach() перебирає всі елементи масиву та виконує передану йому функцію для кожного елемента.
2. За допомогою команди: array.splice(0, array.length); или: array.length = 0;
3. Метод Array.isArray() визначає, чи є передане значення масивом.
*/
const someArray = ['hello', 'world', 547, 23, '23', null, {name: 'John', lastName: 'Smith',}, true, undefined,]
const filterBy = (array, dataType) => {
    return array.filter(item => typeof(item) !== dataType);
};
const arrWithoutStrings = filterBy(someArray, 'string');
console.log('______filter method for string type:_______');
console.log(arrWithoutStrings);

const otherDataTypes = ['number', 'boolean', 'object', 'undefined'];
console.log('______filter method for other data types:_______');
otherDataTypes.forEach(dataType => console.log(filterBy(someArray, dataType)));
//---------------------------forEach method----------------------------------------
const filterByForEach = (array, dataType) => {
    let newArray = [];
    array.forEach((item) => {
        if (typeof(item) !== dataType) {
            newArray.push(item)}
    });
    return newArray;
};
console.log('______forEach method for number type:_______');
console.log(filterByForEach(someArray, 'number'));